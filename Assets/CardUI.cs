﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardUI : MonoBehaviour
{
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI amount;
    public TextMeshProUGUI price;
    public Image icon;

    CardScriptable cardScriptable;

    public void Setup(CardScriptable cardScriptable)
    {
        this.cardScriptable = cardScriptable;
        price.text = cardScriptable.shopType == ShopType.money ? "R$ " + cardScriptable.price + ",00" : cardScriptable.price;
        itemName.text = cardScriptable.itemName;
        amount.text = cardScriptable.amount;
        icon.sprite = cardScriptable.icon;
    }

    public void OpenCardModal()
    {
        ShopModalManager.instance.OpenModal(cardScriptable);
    }
}