﻿using UnityEngine;

[CreateAssetMenu(menuName = "Card", fileName = "Card")]
public class CardScriptable : ScriptableObject
{
    public string itemName;
    public ShopType shopType;
    public string amount;
    public string price;
    public Sprite icon;
}
