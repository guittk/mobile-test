﻿using UnityEngine;
using UnityEngine.Events;

namespace LigaHelpers.Events
{
    public class GameEventBoolListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public GameEventBool Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public BoolEvent Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.UnregisterListener(this);
        }

        public void OnEventRaised(bool value)
        {
            Response.Invoke(value);
        }
    }

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool>
    {

    }
}
