﻿using UnityEngine;
using UnityEngine.Events;

namespace LigaHelpers.Events
{
    public class GameEventFloatListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public GameEventFloat Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public FloatEvent Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.UnregisterListener(this);
        }

        public void OnEventRaised(float value)
        {
            Response.Invoke(value);
        }
    }

    [System.Serializable]
    public class FloatEvent : UnityEvent<float>
    {

    }
}
