﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace LigaHelpers.Events
{
    public class GameEventStringListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public GameEventString Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public StringEvent Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.UnregisterListener(this);
        }

        public void OnEventRaised(string value)
        {
            Response.Invoke(value);
        }
    }

    [System.Serializable]
    public class StringEvent : UnityEvent<string>
    {

    }
}
