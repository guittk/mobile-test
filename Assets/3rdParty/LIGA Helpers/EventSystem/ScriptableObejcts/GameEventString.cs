﻿using System.Collections.Generic;
using UnityEngine;

namespace LigaHelpers.Events
{
    [CreateAssetMenu(menuName = "LIGA Helpers/GameEvents/String")]
    public class GameEventString : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<GameEventStringListener> eventListeners =
            new List<GameEventStringListener>();

        public void Raise(string value)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(value);
        }

        public void RegisterListener(GameEventStringListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(GameEventStringListener listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}
