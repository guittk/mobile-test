﻿using System.Collections.Generic;
using UnityEngine;

namespace LigaHelpers.Events
{
    [CreateAssetMenu(menuName = "LIGA Helpers/GameEvents/Float")]
    public class GameEventFloat : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<GameEventFloatListener> eventListeners =
            new List<GameEventFloatListener>();

        public void Raise(float value)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(value);
        }

        public void RegisterListener(GameEventFloatListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(GameEventFloatListener listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}
