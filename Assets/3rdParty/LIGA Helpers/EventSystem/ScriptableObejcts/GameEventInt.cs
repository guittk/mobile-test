﻿using UnityEngine;
using System.Collections.Generic;

namespace LigaHelpers.Events
{
    [CreateAssetMenu(menuName = "LIGA Helpers/GameEvents/Int")]
    public class GameEventInt : ScriptableObject
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<GameEventIntListener> eventListeners =
            new List<GameEventIntListener>();

        public void Raise(int value)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(value);
        }

        public void RegisterListener(GameEventIntListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(GameEventIntListener listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}
