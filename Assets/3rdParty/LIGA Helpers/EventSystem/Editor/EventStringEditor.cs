﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;

namespace LigaHelpers.Events
{
    [CustomEditor(typeof(GameEventString))]
    public class EventStringEditor : Editor
    {
        private string value;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            // Check para o botão só ficar interagível enquanto o jogo não está rodando
            EditorGUI.BeginDisabledGroup(Application.isPlaying);

            if (GUILayout.Button("Create Interface"))
            {
                // Objecto selecionado na janela Project (O GameEvent em sí)
                Object selectedObject = Selection.activeObject;

                // Pega a primeira letra do nome do objeto selecionado e deixa maiúscula e depois junta com o resto do nome
                string name = selectedObject.name.First().ToString().ToUpper() + selectedObject.name.Substring(1);

                // Nome do arquivo com o I de interface e extensão .cs
                string fileName = "I" + name + ".cs";

                // Caminho do objeto selecionado("Assets/Game/.../.../ScriptableObjects/Objects/<nomeDoEvento>")
                string fullPath = AssetDatabase.GetAssetPath(selectedObject);

                // Tira o nome do arquivo de evento e adiciona o nome do novo arquivo de interface
                string newFilePath = fullPath.Substring(0, fullPath.Length - selectedObject.name.Length - 6) + fileName;

                if (File.Exists(newFilePath) == false)
                { // do not overwrite
                    using (StreamWriter outfile =
                        new StreamWriter(newFilePath))
                    {
                        outfile.WriteLine("public interface I" + name);
                        outfile.WriteLine("{");
                        outfile.WriteLine("    void " + name + "(bool value);");
                        outfile.WriteLine("}");
                    }//File written
                }
            }

            // Atualiza a janela Project do editor pra mostrar o novo arquivo que foi criado
            AssetDatabase.Refresh();

            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(!Application.isPlaying);

            value = EditorGUILayout.TextField(value);
            GameEventString e = target as GameEventString;
            if (GUILayout.Button("Raise"))
                e.Raise(value);

            EditorGUI.EndDisabledGroup();
        }
    }
}
