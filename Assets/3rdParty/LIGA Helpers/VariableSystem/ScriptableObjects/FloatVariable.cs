﻿using System;
using UnityEngine;
using LigaHelpers.Events;

namespace LigaHelpers.Variables
{
    [Serializable]
    [CreateAssetMenu(menuName = "LIGA Helpers/Variables/Float")]
    public class FloatVariable : ScriptableObject
    {
        private float value;
        public float Value
        {
            get
            {
                return value;
            }
            set
            {
                SetValue(value);
            }
        }

        [SerializeField] private GameEventFloat changedEvent;

        public void SetValue(float value)
        {
            this.value = value;
            if (changedEvent != null)
            {
                changedEvent.Raise(value);
            }
        }

        public static implicit operator float(FloatVariable variable)
        {
            return variable.Value;
        }
    }
}
