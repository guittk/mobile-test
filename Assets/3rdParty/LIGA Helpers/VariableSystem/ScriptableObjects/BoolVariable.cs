﻿using System;
using UnityEngine;
using LigaHelpers.Events;

namespace LigaHelpers.Variables
{
    [Serializable]
    [CreateAssetMenu(menuName = "LIGA Helpers/Variables/Bool")]
    public class BoolVariable : ScriptableObject
    {
        private bool value;
        public bool Value
        {
            get
            {
                return value;
            }
            set
            {
                SetValue(value);
            }
        }

        [SerializeField] private GameEventBool changedEvent;

        public void SetValue(bool value)
        {
            this.value = value;
            if(changedEvent != null)
            {
                changedEvent.Raise(value);
            }
        }

        public void Toggle()
        {
            SetValue(!Value);
        }

        public static implicit operator bool(BoolVariable variable)
        {
            return variable.Value;
        }
    }
}
