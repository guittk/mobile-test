﻿using System;
using UnityEngine;
using LigaHelpers.Events;

namespace LigaHelpers.Variables
{
    [Serializable]
    [CreateAssetMenu(menuName = "LIGA Helpers/Variables/String")]
    public class StringVariable : ScriptableObject
    {
        private string value;
        public string Value
        {
            get
            {
                return value;
            }
            set
            {
                SetValue(value);
            }
        }

        [SerializeField] private GameEventString changedEvent;

        public void SetValue(string value)
        {
            this.value = value;
            if (changedEvent != null)
            {
                changedEvent.Raise(value);
            }
        }

        public static implicit operator string(StringVariable variable)
        {
            return variable.Value;
        }
    }
}
