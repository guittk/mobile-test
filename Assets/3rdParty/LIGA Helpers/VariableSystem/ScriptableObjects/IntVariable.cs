﻿using System;
using UnityEngine;
using LigaHelpers.Events;

namespace LigaHelpers.Variables
{
    [Serializable]
    [CreateAssetMenu(menuName = "LIGA Helpers/Variables/Int")]
    public class IntVariable : ScriptableObject
    {
        private int value;
        public int Value
        {
            get
            {
                return value;
            }
            set
            {
                SetValue(value);
            }
        }

        [SerializeField] private GameEventInt changedEvent;

        public void SetValue(int value)
        {
            this.value = value;
            if (changedEvent != null)
            {
                changedEvent.Raise(value);
            }
        }

        public static implicit operator int(IntVariable variable)
        {
            return variable.value;
        }
    }
}
