﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum ShopType { coins, gems, money };

[System.Serializable]
public struct ShopTab
{
    public string name;
    public List<CardScriptable> shopItems;
}

public class Shop : MonoBehaviour
{
    public List<ShopTab> shopTabs;

    [Space]
    public GameObject cardPrefab;
    public GameObject titlePrefab;
    public GameObject cardsContentPrefab;
    public RectTransform content;

    void Start()
    {
        CreateShop();
    }

    void CreateShop()
    {
        float contentSizeY = 0;

        // Instancia todas as abas e cards da loja
        foreach (var t in shopTabs)
        {
            // Instancia e poem nome do titulo da aba
            RectTransform title = Instantiate(titlePrefab, Vector3.zero, Quaternion.identity, content.transform).GetComponent<RectTransform>(); ;
            title.GetComponentInChildren<TextMeshProUGUI>().text = t.name;

            // Instancia e ajusta tamanho do content da aba
            RectTransform cardsContent = Instantiate(cardsContentPrefab, Vector3.zero, Quaternion.identity, content.transform).GetComponent<RectTransform>();
            int linesOfCards = Mathf.CeilToInt(t.shopItems.Count / 3f);
            cardsContent.sizeDelta = new Vector2(cardsContent.sizeDelta.x, linesOfCards * 365);

            // Calcula o tamanho do content da loja inteira
            contentSizeY += title.sizeDelta.y + cardsContent.sizeDelta.y;

            // Instancia e poem os valores nos cards da loja
            foreach (var i in t.shopItems)
            {
                CardUI card = Instantiate(cardPrefab, Vector3.zero, Quaternion.identity, cardsContent.transform).GetComponent<CardUI>();
                card.price.text = i.shopType == ShopType.money ? "R$ " + i.price + ",00" : i.price;
                card.itemName.text = i.itemName;
                card.amount.text = i.amount;
                card.icon.sprite = i.icon;
                card.id = i.id;
            }
        }

        // Ajusta o tamanho do content da loja inteira
        content.sizeDelta = new Vector2(content.sizeDelta.x, contentSizeY);
    }
}