﻿using UnityEngine;
using TMPro;

public class ShopModalManager : MonoBehaviour
{
    public static ShopModalManager instance;

    public TextMeshProUGUI modalTitle;
    public TextMeshProUGUI modalPrice;
    public Animator animator;
    public CardUI cardUI;

    void Awake()
    {
        instance = this;
    }

    public void OpenModal(CardScriptable cardScriptable)
    {
        animator.Play("ShopModalOpen");
        modalTitle.text = "Adquirir " + cardScriptable.itemName + "?";
        modalPrice.text = cardScriptable.price;
        cardUI.Setup(cardScriptable);
    }

    public void CloseModal()
    {
        animator.Play("ShopModalClose");
    }
}